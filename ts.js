module.exports = {
  env: {
    'browser': true,
    'commonjs': true,
    'es6': true
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'airbnb-base'
  ],
  plugins: ['@typescript-eslint'],
  globals: {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly'
  },
  settings: {
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx']
    },
    'import/resolver': {
      'node': {
        'extensions': ['.js', '.jsx', '.ts', '.tsx', '.d.ts']
      }
    }
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    'ecmaVersion': 2018,
    'project': './tsconfig.json'
  },
  rules: {
    'arrow-parens': ['error', 'always'],
    'prefer-const': 2,
    'no-process-exit': 0,
    'no-magic-numbers': ['error', {
      'ignore': [-1, 0, 1, 10, 100, 1000]
    }],
    'func-style': ['warn', 'declaration', { 'allowArrowFunctions': true }],
    'prefer-destructuring': ['off'],
    'curly': 'error',
    'max-len': ['error', 130],
    'brace-style': ['error', 'stroustrup'],
    'nonblock-statement-body-position': ['error', 'below'],
    'quotes': ['error', 'single'],
    'prefer-promise-reject-errors': 0,
    'valid-jsdoc': 'warn',
    'no-eval': 'error',
    'no-plusplus': 0,
    'no-bitwise': 0,
    'no-param-reassign': 1,
    'operator-linebreak': ['error', 'after'],
    'newline-before-return': 'error',
    'newline-after-var': 'error',
    'no-underscore-dangle': 'off',
    'padding-line-between-statements': [
      'error',
      {
        'blankLine': 'always',
        'prev': ['block'],
        'next': '*'
      }
    ],
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        'js': 'never',
        'jsx': 'never',
        'ts': 'never',
        'tsx': 'never'
      }
    ]
  }
};